#!/bin/bash

# download proto dependencies
protodep up

# compile descriptor set
protoc \
  --proto_path=./ \
  --proto_path=./protos \
  --descriptor_set_out=./hello.pb \
  --include_imports \
  --include_source_info \
  hello.proto

# copy generated descriptor set to envoy dir
cp hello.pb ../envoy/
