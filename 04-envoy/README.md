# Envoy gRPC-JSON transcoder

### Build and Run gRPC Service and Envoy Proxy

```bash
docker compose up
```

## Test

```bash
# Send GET request to /say-hello
curl --silent --show-error --header Content-Type:application/json --request GET http://localhost:20000/say-hello?name=World | jq

# Create a todo
curl --silent --show-error --header Content-Type:application/json --data '{"title": "prepare brown bag session"}' --request POST http://localhost:20000/todos | jq

# List all todos
curl --silent --show-error --header Content-Type:application/json --request GET http://localhost:20000/todos | jq
```
