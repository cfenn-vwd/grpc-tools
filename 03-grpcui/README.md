# gRPC UI

## Preparations

### Start gRPC Service

```bash
cd hello-service

# Download proto dependencies and compile descriptor set
./compile-descriptor-set.sh

# Download dependencies
npm install

# Start gRPC server
npm start
```

## Usage

```bash
# Download binary from https://github.com/fullstorydev/grpcui/releases
# and put it to a directory in your PATH

# Start gRPC UI on port 8080 using plaintext protos
grpcui -port 8080 -proto ../hello-service/hello.proto -import-path ../hello-service -import-path ../hello-service/protos -plaintext localhost:12345

# Start gRPC UI on port 8080 using descriptor set
grpcui -port 8080 -protoset ../hello-service/descriptor_set.pb -plaintext localhost:12345

# Start gRPC UI with some example requests
grpcui -port 8080 -protoset ../hello-service/descriptor_set.pb -plaintext  -examples examples.json localhost:12345

# Start gRPC UI exposing just some methods
grpcui -port 8080 -protoset ../hello-service/descriptor_set.pb -plaintext -method hellopackage.GreeterService.SayHello localhost:12345

# Start gRPC UI loading some custom CSS styles
grpcui -port 8080 -protoset ../hello-service/descriptor_set.pb -plaintext -extra-css my-style.css localhost:12345
```
