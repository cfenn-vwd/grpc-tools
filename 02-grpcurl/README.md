# gRPCurl

## Preparations

### Start gRPC Service

```bash
cd hello-service

# Download proto dependencies and compile descriptor set
./compile-descriptor-set.sh`

# Download dependencies
npm install

# Start gRPC server
npm start
```

## Usage
Download binary from https://github.com/fullstorydev/grpcurl/releases
and put it to a directory in your PATH

Send some requests via grpcurl. See `test-grpcurl.sh`.