#!/bin/bash
set -uo errexit

ADDRESS="localhost:12345"
PROTO_PACKAGE='hellopackage'
PROTO_SERVICE='GreeterService'

# call SayHello rpc with plaintext proto
PROTO_METHOD='SayHello'
PROTO_PATH="${PROTO_PACKAGE}.${PROTO_SERVICE}.${PROTO_METHOD}"
DATA='{
  "name": "Infront"
}'
grpcurl -proto ../hello-service/hello.proto -plaintext -d "${DATA}" ${ADDRESS} ${PROTO_PATH} | jq

# call SayHello rpc with descriptor set
PROTO_METHOD='SayHello'
PROTO_PATH="${PROTO_PACKAGE}.${PROTO_SERVICE}.${PROTO_METHOD}"
DATA='{
  "name": "Proto Descriptor Set"
}'
grpcurl -protoset ../hello-service/descriptor_set.pb -plaintext -d "${DATA}" ${ADDRESS} ${PROTO_PATH} | jq
