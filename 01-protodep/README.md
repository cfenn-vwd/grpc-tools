# protodep

## Usage

```bash
# Download binary from https://github.com/stormcat24/protodep/releases
# and put it to a directory in your PATH

protodep --help

# download proto dependencies considering lockfile
protodep up

# download proto dependecies ignoring/rebuilding lockfile
protodep up --force
```

Then do whatever you want with the protos. E.g. compile a descriptor set with protoc:

```bash
protoc \
  --proto_path=./protos \
  --descriptor_set_out=./descriptor_set.pb \
  --include_imports \
  dev/infrontfinance/regfeed/result/v2/service.proto
```