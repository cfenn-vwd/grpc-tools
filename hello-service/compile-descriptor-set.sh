#!/bin/bash

# compile descriptor set
protoc \
  --proto_path=./ \
  --descriptor_set_out=./descriptor_set.pb \
  --include_imports \
  --include_source_info \
  hello.proto
