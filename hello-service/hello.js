'use strict'

const http = require('http')
const path = require('path')
const grpc = require('grpc')
const protoLoader = require('@grpc/proto-loader')

const grpcPort = 12345

const protoPath = path.resolve(__dirname, 'hello.proto')
const packageDefinition = protoLoader.loadSync(protoPath, {
  keepCase: true,
  defaults: false,
})
const protoDescriptor = grpc.loadPackageDefinition(packageDefinition)

const methods = {
  SayHello (call, callback) {
    callback(null, {
      message: `Hello ${call.request.name}`,
    })
  },
}
const server = new grpc.Server()
server.addService(protoDescriptor.hellopackage.GreeterService.service, methods)

console.log(`Starting greeter service gRPC`)
if (server.bind(`0.0.0.0:${grpcPort}`, grpc.ServerCredentials.createInsecure()) === 0) {
  throw new Error(`Address already in use: 0.0.0.0:${grpcPort}`)
}
server.start()
console.log(`Greeter service gRPC is listening on port ${grpcPort}`)
